﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassDemo
{
    public class Stock
    {
        private List<ProductList> stockList;

        public Stock()
        {
            this.StockList = new List<ProductList>();
        }

        public Stock(List<ProductList> productList)
        {
            this.StockList = productList;
        }

        public List<ProductList> StockList
        {
            set { stockList = value; }
            get { return stockList;  }
        }
    }
}
