﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassDemo
{
    public class ProductList
    {
        private string productType;
        private List<Product> productList;


        public ProductList()
        {
            this.productList = new List<Product>();
        }

        public ProductList(string productTypeName, List<Product> productList)
        {
            this.ProductType = productTypeName;
            this.Products = productList;
        }

        public string ProductType
        {
            get { return productType; }
            set { productType = value; }
        }

        public List<Product> Products
        {
            get { return productList; }
            set { productList = value; }
        }
    }
}
