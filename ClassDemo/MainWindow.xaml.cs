﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Newtonsoft.Json;// Add package for JSON
using System.IO; // Read/write files

namespace ClassDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public Stock stockList = new Stock();

        public Stock stockListCopy = new Stock();

        public MainWindow()
        {
            InitializeComponent();

            FillLists();

            SerialiseToFile("demo.json", stockList);

            DeserialiseFromFile("demo.json", ref stockListCopy);

        }



        private void SerialiseToFile(string FileName, Stock listofStock)
        {
            // serialize JSON directly to a file
            using (StreamWriter file = File.CreateText(@FileName))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, listofStock);
            } // end using Stream writer
        } // end serialise and save

        private void DeserialiseFromFile(string FileName, ref Stock listOfStock)
        {
            using (StreamReader file = File.OpenText(FileName))
            {
                String jsonText = File.ReadAllText(@FileName);
                Stock tempStockList = JsonConvert.DeserializeObject<Stock>(jsonText);
                listOfStock = tempStockList;
            } // end stream read

            
        }

        public void FillLists()
        {
            // Create a new product list entry for fruit
            ProductList aList = new ProductList();
            aList.ProductType = "Fruit";

            // add products
            Product aProduct = new Product();
            aProduct.ProductName = "Apple";
            aList.Products.Add(aProduct);

            aProduct = new Product();
            aProduct.ProductName = "Banana";
            aList.Products.Add(aProduct);

            // add product list to stock list
            stockList.StockList.Add(aList);



            aList = new ProductList();
            aList.ProductType = "Veggies";

            aProduct = new Product();
            aProduct.ProductName = "Potato";
            aList.Products.Add(aProduct);

            aProduct = new Product();
            aProduct.ProductName = "Turnip";
            aList.Products.Add(aProduct);

            stockList.StockList.Add(aList);



            aList = new ProductList();
            aList.ProductType = "Seafood";

            aProduct = new Product();
            aProduct.ProductName = "Crab";
            aList.Products.Add(aProduct);

            aProduct = new Product();
            aProduct.ProductName = "Salmon";
            aList.Products.Add(aProduct);


            aProduct = new Product();
            aProduct.ProductName = "Shark";
            aList.Products.Add(aProduct);

            aProduct = new Product();
            aProduct.ProductName = "Lobster";
            aList.Products.Add(aProduct);

            stockList.StockList.Add(aList);
        }


    }
}
